package po;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.util.List;

public class GmailHomePagePO extends BasePage {
    public GmailHomePagePO(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[@class='z0']/*[@role='button']")
    WebElement writeAMessageButton;
    @FindBy(name = "to")
    WebElement writeAMessageToLine;
    @FindBy(name = "subjectbox")
    WebElement subjectLine;
    @FindBy(xpath = "//div[@class='Ar Au']/*[@role='textbox']")
    WebElement writeAMessageBox;
    @FindBy(xpath = "//div[@class='dC']/*[@role='button']")
    WebElement sendAMessageButton;
    @FindBy(xpath = "//div[@class = 'TK']/div[position()=4]/div/div/div[2]")
    WebElement sentMessagesButton;
    @FindBy(xpath =  "//div[@role='main']//tr[@jscontroller = 'ZdOxDb']")
    List<WebElement> messagesList;


    @FindBy(xpath = "//div[@style='display:']")
    WebElement messageDetails;


    public void clickOnWriteAMessageButton() {
        writeAMessageButton.click();
    }

    public void writeAMessageTo(String email) {
        writeAMessageToLine.sendKeys(email);
    }

    public void writeASubjectOfMessage(String subject) {
        subjectLine.sendKeys(subject);
    }

    public void writeAMessage(String message) {
        writeAMessageBox.sendKeys(message);
    }

    public void sendAMessage() {
        sendAMessageButton.click();
    }

    public void clickOnSentMessagesButton() {
        driver.switchTo().defaultContent();
        sentMessagesButton.click();
    }

    public void openLastMessage(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        messagesList.get(0).click();
    }

    public String getMessageDetails(){
        return messageDetails.getText();
    }
}

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import po.GmailHomePagePO;
import po.GmailLoginPO;

import java.util.concurrent.TimeUnit;

public class GmailTest {
    WebDriver driver = new DriverManager().getDriver();

    @Test
    public void testGmail() {
        driver.get("https://mail.google.com");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        GmailLoginPO gmailLoginPO = new GmailLoginPO(driver);
        gmailLoginPO.inputLoginAndClick("voronovskatest@gmail.com");
        gmailLoginPO.inputPasswordAndClick("mino1234");
        GmailHomePagePO gmailHomePagePO = new GmailHomePagePO(driver);
        gmailHomePagePO.clickOnWriteAMessageButton();
        gmailHomePagePO.writeAMessageTo("voronovskaira7@gmail.com");
        gmailHomePagePO.writeASubjectOfMessage("Selenium");
        gmailHomePagePO.writeAMessage("I like Selenium");
        gmailHomePagePO.sendAMessage();
        gmailHomePagePO.clickOnSentMessagesButton();
        gmailHomePagePO.openLastMessage();
        Assert.assertTrue(gmailHomePagePO.getMessageDetails().contains("voronovskaira7"));
        Assert.assertTrue(gmailHomePagePO.getMessageDetails().contains("I like Selenium"));
        Assert.assertTrue(gmailHomePagePO.getMessageDetails().contains("voronovskatest@gmail.com"));
    }

    @AfterTest
    public void tearDown() {
        driver.quit();
    }
}

